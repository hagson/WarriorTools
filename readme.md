# Introduction

WarriorTools is an addon to automate some warrior bussiness allowing the player to focus on other things, and to help optimize their gameplay.

The core automation is provided by a function called PerformRotation. This function can be modified by passing a table as the paramater containing the values you want to modify. Detailed explanation is available in a later section.

A number of useful functions are provided that are used throughout the program, but that also are useful on their own, such as TimeUntilMHLands(), SpellCooldown(spellName) etc. A full API is provided at the end of this document.


# Usage

The main usage will be through the slash command /wt command [option]
Command specifies what to do, option is an optional parameter that modifies the default behaviour.
Available commands are:

### dps [options]
Calls PerformRotation. If options is provided this is converted into a table and passed as a parameter. For a full list of options and details on how PerformRotation works see the next section of this document. Examples:

/wt dps
Performs the default DPS rotation

/wt dps doAnni=false, doHeroicStrike=true, HSRageThresh=60
Performs the default DPS rotation, but without equipping Annihilator when needed, including Heroic Strike in the rotation, and increases the threshold for when to Heroic Strike from the default to 60.

### sunder
Calls PerformRotation in such a way that the only spell it uses is Sunder Armor, when appropriate.

### annihilator [weapon]
Calls PerformRotation in such a way that it only swaps to and from Annihilator when appropriate. If weapon is provided this weapon will be equiped when removing annihilator, otherwise the weapon used before annihilator will be equipped.

### debuffs [weapon]
Sunders and equips/unequips annihilator as described above.


# Detailed information about PerformRotation

By default it attempts to emulate the following rotation, with a couple of differences:

>The rotation:
>1) Can you execute? Then just spam Execute.
>2) Can you BT? Use BT.
>3) Is BT cooldown longer than GCD and can you WW? Then WW.
>4) Is BT cooldown longer than GCD and can you overpower, and do you have at most 50 rage? Then swap the stance and use overpower.
>5) Is BT cooldown longer than GCD, has MH swing timer just been reset, and do you have at least 25 rage? Then hamstring.
>
>HS related:
>1) Is your swing timer about to end and do you have at least 35 rage? Then toggle HS fast before the strike lands.
>
>And lastly:
>1) Are you currently still in battle stance and either have less than 25 rage anyway or have more but swing timer of MH will land before BT is off cooldown? Then go to berserker stance.

## Differences and interpretations
* Sunder is enabled by default in the rotation, and has highest priority except weapon swaping.
* Swapping to and from annihilator is supported and enabled by default.
* Starting Auto Attack if not already enabled is available and enabled by default.
* Using Heroic Strike is disabled by default
* Overpower takes priority over Whirlwind, assuming, of course, it's conditions apply.
* If you have little rage (15 or less by default) and MH won't land within the next GCD Overpower will be cast despite Bloodthirst not having less than 1.5s cooldown
* Being in execute phase doesn't rule out using Heroic Strike
* Being in Battle Stance makes the rage critera for Overpower not apply
* Being about to cast Whirlwind forces a stance swap regardless of rage and swingtimer
* "MH swing timer just been reset" is defined as "80% or more of the swing time remains". This value can modified by passing the option hamstringSwingThresh
* "Swing timer about to end" is defined as having 0.3 seconds or less left of the swing timer. This value can modified by passing the option HSSwingThresh

## Full list of options
The options are described on the form **optionName=defaultValue**
Any values of the same type (eg. int, boolean) as the default will be accepted.

**doStartAttack=true**
Starts autoattacking if not active

**doExecute=true**
Uses Execute as part of the rotation

**doBloodthirst=true**
Uses Bloodthirst as part of the rotation

**doWhirlwind=true**
Uses Whirlwind as part of the rotation

**doHamstring=true**
Uses Hamstring as part of the rotation

**doOverpower=true**
Uses Overpower as part of the rotation, including switching to Battle Stance

**doHeroicStrike=false**
Uses Heroic Strike as part of the rotation

**doSwitchStance=false**
Switches to Berserker Stance if you are not in it and it makes sense to do so. Conditions can be modified. No effect if doOverpower is enabled

**doSunder=true**
Uses sunder if target has more than sunderThresh HP and doesn't have full stacks of sunder

**sunderThresh=25000**
The amount of health the target needs to have for it to be worth it to sunder the target

**doAnni=true**
Switches to annihilator if target has more than anniThresh HP and less than two stacks of armor shatter, and back if it does not and you're using anni

**anniThresh=450000**
The amount of health the target needs to have for it to be worth it to equip annihilator

**doLIPIfTargetMe=false**
Uses Limited Invulnerability if your target is targeting you

**OPRageThresh=50**
If you have more rage than this, and you're not in Battle Stance, Overpower will not be used (since swapping stance would be a waste of rage)

**OPTimeThresh=0.1**
If you have less than this amount of time left of the Overpower window and you're not in Battle Stance Overpower will not be attempted

**OPAnywayThresh=15**
If you have less rage than this and MH won't land within the next GCD Overpower will be cast despite Bloodthirst not having less than 1.5s cooldown

**hamstringRageThresh=25**
If you have less rage than this Hamstring will not be used

**hamstringSwingThresh=0.8\*UnitAttackSpeed("player")**
If you have less than this amount of time until MH swing lands Hamstring will not be used

**HSRageThresh=35**
If you have less rage than this Heroic Strike will not be used

**HSSwingThresh=0.3**
If you have more than this amount of time until MH swing lands Heroic Strike will not be used


# API

**GetSpellbookSlotFromName(spell)**
Returns the slot in the spellbook of the spell provided as argument. If it can not be found a warning is printed and nil returned.

**TargetNeedsDebuff(title, description, HPThreshold, blacklist, whitelist)**
Returns true if the target doesn't have a debuff with the title title and description description and either is on the whitelist, or has more than HPThreshold HP and is not on the blacklist. Relies on the MobInfo2 addon.

**TargetNeedsSunder(HPThreshold, blacklist, whitelist)**
TargetNeedsDebuff with title and description matching 5 stacks of Sunder Armor. Relies on the MobInfo2 addon.

**TargetNeedsAnnihilator(HPThreshold, blacklist, whitelist)**
Special version of TargetNeedsDebuff for Annihilator that returns false when target has 2 *or* 3 stacks. Relies on the MobInfo2 addon.

**SpellCooldown(spell)**
Returns the cooldown of the spell with the name of the spell provided as argument.

**AmAutoAttacking()**
Returns true if you are auto attacking, false otherwise. Requires Attack to be somewhere on the action bars, and will print a warning and return nil if it is not.

**StartAutoAttacking()**
Starts auto attack if you are not already doing so. Requires Attack to be somewhere on the action bars, and will print a warning and return nil if it is not.

**CanExecute()**
Returns true if target is in execute range, false otherwise.

**CanOverpower(timeThresh)**
Returns true if you can Overpower for timeThresh seconds or more. Relies on the SP_Overpower addon.

**TimeUntilMHWillLand()**
Returns the time until your Main Hand swing will land. Relies on the VGAttackBar addon.

**LipIfTargetingMe()**
Uses Limited Invulnerability Potion if your target is targeting you. Relies on the SuperMacro addon.
