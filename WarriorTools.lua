-- Copyright 2018 Hagson

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

CreateFrame("GameTooltip","WT_TLTP",nil,"GameTooltipTemplate")
WT_TLTP:SetOwner(WorldFrame,"ANCHOR_NONE")
WT_TLTP:SetScript("OnEvent", function() WT_onEvent() end)
WT_TLTP:RegisterEvent("ADDON_LOADED")

SLASH_WT1 = "/wt"
SlashCmdList["WT"] = function(msg) WT_onSlash(msg) end

function WT_onEvent()
	if event == "ADDON_LOADED" and arg1 == "WarriorTools" then
		if WT == nil then
			WT = {}
		end
		WT.sunder_blacklist = {}
		WT.sunder_whitelist = { "Undercity Practice Dummy", "Ragnaros", "Vaelastrasz the Corrupt", "Nefarian" }
		WT.annihilator_blacklist = { }
		WT.annihilator_whitelist = { "Undercity Practice Dummy", "Ragnaros", "Vaelastrasz the Corrupt", "Nefarian" }
		WT.slotCache = {}
		DEFAULT_CHAT_FRAME:AddMessage("WarriorTools loaded")
	end
end


function WT_onSlash(msg)
	s,e = string.find(msg, " ")
	if s then
		command = string.sub(msg,0,s-1)
		option = string.sub(msg,e+1)
	else
		command = msg
	end
	if command == "dps" then
		if option then
			valstart = string.find(option, "=")
			if not s then
				DEFAULT_CHAT_FRAME:AddMessage("Option parameter malformed. Syntax is /wt dps opt1=val, opt2=val etc")
				return
			end
			opts = {}
			optstart = 0
			while valstart do
				opt = string.sub(option,optstart,valstart-1)
				val_end = string.find(option, ", ", valstart)
				if not val_end then -- Last option
					val = string.sub(option,valstart+1)
				else
					val = string.sub(option,valstart+1,val_end-1)
				end
				if val == "true" then
					val = true
				elseif val == "false" then
					val = false
				elseif tonumber(val) then
					val = tonumber(val)
				else
					DEFAULT_CHAT_FRAME:AddMessage("Warning: Value \"" .. val .. "\" is neither numeric nor boolean, are you sure it's correct?")
				end
				opts[opt] = val
				if val_end then
					optstart = val_end + 2 -- Remove ", "
				end
				valstart = string.find(option, "=", valstart+1)
			end
			PerformRotation(opts)
		else
			PerformRotation()
		end
	elseif command == "sunder" then
		PerformRotation{doStartAttack=false, doExecute=false, doBloodthirst=false, doWhirlwind=false, doHamstring=false, doSwitchStance=false, doAnni=false} -- Only sunder
	elseif command == "annihilator" then
		if option then
			WT.myWeapon = option
		end
		PerformRotation{doStartAttack=false, doExecute=false, doBloodthirst=false, doWhirlwind=false, doHamstring=false, doSwitchStance=false, doSunder=false} -- Only anni
	elseif command == "debuffs" then
		if option then
			WT.myWeapon = option
		end
		PerformRotation{doStartAttack=false, doExecute=false, doBloodthirst=false, doWhirlwind=false, doHamstring=false, doSwitchStance=false} -- Only anni and sunder
	else
		DEFAULT_CHAT_FRAME:AddMessage("Unkown command \"" .. command .. "\". Available commands are:")
		DEFAULT_CHAT_FRAME:AddMessage("dps [options] - Performs DPS rotation. Can be modified by options. See readme for how")
		DEFAULT_CHAT_FRAME:AddMessage("sunder - Sunders if target needs sunder")
		DEFAULT_CHAT_FRAME:AddMessage("annihilator [weapon] - Equips annihilator if target needs annihilator, otherwise it equips whatever was unequipped when equipping annihilator. If weapon is specified that is equiped when annihilator is no longer needed.")
		DEFAULT_CHAT_FRAME:AddMessage("debuffs [weapon] - Sunders and equips/unequips annihilator as described above.")
	end
end

function GetSpellbookSlotFromName(spell)
	local slot = nil
	if WT.slotCache[spell] then
		return WT.slotCache[spell]
	else
		local i = 1
		local spellName = GetSpellName(1, "spell")
		while spellName do
			if spellName == spell then
				WT.slotCache[spell] = i
				return i
			end
			i=i+1
			spellName = GetSpellName(i, "spell")
		end
		DEFAULT_CHAT_FRAME:AddMessage("Couldn't find spell " .. spell .. " in your spellbook. Have you trained it?")
		return nil
	end
end

function TargetNeedsDebuff(title, description, HPThreshold, blacklist, whitelist)
	if not UnitExists("target") then return false end
	local preemptive = true

	if (MobHealth_GetTargetCurHP == nil) then
		preemptive = false
	elseif (MobHealth_GetTargetCurHP() == nil) then
		preemptive = false
	elseif (MobHealth_GetTargetCurHP() < HPThreshold) then
		preemptive = false
	end
	-- if (not MobHealth_GetTargetCurHP or not MobHealth_GetTargetCurHP() or MobHealth_GetTargetCurHP() < HPThreshold) then
	-- 	preemptive = false
	-- end

	for i=1,getn(blacklist) do
		if UnitName("target") == blacklist[i] then
			preemptive = false
		end
	end

	for i=1,getn(whitelist) do
		if UnitName("target") == whitelist[i] then
			preemptive = true
		end
	end

	local i=1
	while UnitDebuff("target",i) do
		WT_TLTP:SetUnitDebuff("target",i)
		if (WT_TLTPTextLeft1:GetText() == title and WT_TLTPTextLeft2:GetText() == description) then
			preemptive = false
		end
		i=i+1
	end

	return preemptive
end

function TargetNeedsSunder(HPThreshold, blacklist, whitelist)
	return TargetNeedsDebuff("Sunder Armor", "Armor decreased by 2250.", HPThreshold, blacklist, whitelist)
end

function TargetNeedsAnnihilator(HPThreshold, blacklist, whitelist)
	-- return TargetNeedsDebuff("Armor Shatter", "Armor reduced by 600.", WT.annihilator_hpthresh, WT.annihilator_blacklist, WT.annihilator_whitelist)
	if not UnitExists("target") then return false end
	local preemptive = true

	if (MobHealth_GetTargetCurHP == nil) then
		preemptive = false
	elseif (MobHealth_GetTargetCurHP() == nil) then
		preemptive = false
	elseif (MobHealth_GetTargetCurHP() < HPThreshold) then
		preemptive = false
	end
	-- if (not MobHealth_GetTargetCurHP or not MobHealth_GetTargetCurHP() or MobHealth_GetTargetCurHP() < HPThreshold) then
	-- 	preemptive = false
	-- end

	for i=1,getn(blacklist) do
		if UnitName("target") == blacklist[i] then
			preemptive = false
		end
	end

	for i=1,getn(whitelist) do
		if UnitName("target") == whitelist[i] then
			preemptive = true
		end
	end

	local i=1
	while UnitDebuff("target",i) do
		WT_TLTP:SetUnitDebuff("target",i)
		if (WT_TLTPTextLeft1:GetText() == "Armor Shatter" and (WT_TLTPTextLeft2:GetText() == "Armor reduced by 600." or WT_TLTPTextLeft2:GetText() == "Armor reduced by 400.")) then
			preemptive = false
		end
		i=i+1
	end

	return preemptive
end

function SpellCooldown(spell)
	-- 	local slot = GetSlotFromName(spell)
	local slot = GetSpellbookSlotFromName(spell)
	if not slot then DEFAULT_CHAT_FRAME:AddMessage("Failed to get cooldown of " .. spell) return nil end
	local s, d, e = GetSpellCooldown(slot, "spell")
	if s ~= 0 then
		return s-GetTime()+d
	else
		return 0
	end
end

function AmAutoAttacking()
	local slot = nil
	if WT.slotCache["Attack"] then
		slot = WT.slotCache["Attack"]
	else
		for i=1,120 do
			if IsAttackAction(i) then
				slot = i
				WT.slotCache["Attack"] = i
				break
			end
		end
	end
	if slot then
		return IsCurrentAction(slot)
	else
		DEFAULT_CHAT_FRAME:AddMessage("In order to determine whether you're attacking the attack action needs to be on your bars somewhere (it may be hidden)")
		return nil
	end
end

function StartAutoAttack()
	if not AmAutoAttacking() then -- This check adds attack to the slotCache, or prints a warning if Attack is not on the bars.
		if WT.slotCache["Attack"] then
			UseAction(WT.slotCache["Attack"])
		end
	end
end

function CanExecute()
	if UnitExists("target") then
		return UnitHealth("target")/UnitHealthMax("target") <= 0.2
	else
		return false
	end
end

function CanOverpower(timeThresh) -- Relies on SPOverpower
	_,_,IsInBattleStance = GetShapeshiftFormInfo(1)
	if SP_OP_TimeLeft then
		if IsInBattleStance then
			return SP_OP_TimeLeft > 0
		else
			return SP_OP_TimeLeft > timeThresh
		end
	else
		return false
	end
end

function TimeUntilMHWillLand() -- Relies on VGAB
	if VGAB_Mhr.et then
		if VGAB_Mhr.et-GetTime() < 0 then
			return 0
		else
			return VGAB_Mhr.et-GetTime()
		end
	else
		return 0
	end
end

function LipIfTargetingMe()
	if UnitName("targettarget") == UnitName("player") then
		UseItemByName("Limited Invulnerability Potion")
	end
end

function PerformRotation( options ) -- Call with PerformRotation{doExecute=false, doOverpower=true, HSRageThresh=60} etc to change default values
	if options == nil then options = {} end -- Set default values
	if options.doStartAttack == nil then	options.doStartAttack = true	end -- Starts autoattacking if not active
	if options.doExecute == nil then		options.doExecute = true		end -- Uses Execute as part of the rotation
	if options.doBloodthirst == nil then	options.doBloodthirst = true	end -- Uses Bloodthirst as part of the rotation
	if options.doWhirlwind == nil then		options.doWhirlwind = true		end -- Uses Whirlwind as part of the rotation
	if options.doHamstring == nil then		options.doHamstring = true		end -- Uses Hamstring as part of the rotation
	if options.doOverpower == nil then		options.doOverpower = true		end -- Uses Overpower as part of the rotation, including switching to Battle Stance
	if options.doHeroicStrike == nil then	options.doHeroicStrike = false	end -- Uses Heroic Strike as part of the rotation
	if options.doSwitchStance == nil then	options.doSwitchStance = false	end -- Switches to Berserker Stance if you are not in it and it makes sense to do so. Conditions can be modified. No effect if doOverpower is enabled
	if options.doSunder == nil then 		options.doSunder = true			end -- Uses sunder if target has more than sunderThresh HP and doesn't have full stacks of sunder
	if options.sunderThresh == nil then		options.sunderThresh = 25000	end -- The threshold at which it's worth it to sunder the target
	if options.doAnni == nil then			options.doAnni = true			end -- Switches to annihilator if target has more than anniThresh HP and less than two stacks of sunder, and back if it's not and you're using anni
	if options.anniThresh == nil then		options.anniThresh = 450000		end -- The threshold at which it's worth it to equip annihilator
	if options.doLIPIfTargetMe == nil then	options.doLIPIfTargetMe = false	end -- Uses Limited Invulnerability if your target is targeting you

	if options.OPRageThresh == nil then			options.OPRageThresh = 50 			end -- If you have more rage than this, and you're not in Battle Stance, Overpower will not be used (since swapping stance would be a waste of rage)
	if options.OPTimeThresh == nil then			options.OPTimeThresh = 0.1			end -- If you have less than this amount of time left of the Overpower window and you're not in Battle Stance Overpower will not be attempted
	if options.OPAnywayThresh == nil then		options.OPAnywayThresh = 15			end -- If you have less rage than this and MH won't land within the next GCD Overpower will be cast despite Bloodthirst not having less than 1.5s cooldown
	if options.hamstringRageThresh == nil then	options.hamstringRageThresh = 25	end -- If you have less rage than this Hamstring will not be used
	if options.hamstringSwingThresh == nil then	options.hamstringSwingThresh = 0.8*UnitAttackSpeed("player") end -- If you have less than this amount of time until MH swing lands Hamstring will not be used
	if options.HSRageThresh == nil then			options.HSRageThresh = 35			end -- If you have less rage than this Heroic Strike will not be used
	if options.HSSwingThresh == nil then		options.HSSwingThresh = 0.3			end -- If you have more than this amount of time until MH swing lands Heroic Strike will not be used


	if options.doStartAttack then StartAutoAttack() end

	if options.doLIPIfTargetMe then LipIfTargetingMe() end

	if options.doHeroicStrike and TimeUntilMHWillLand() < options.HSSwingThresh and UnitMana("player") > options.HSRageThresh then
		-- DEFAULT_CHAT_FRAME:AddMessage("WT: Heroically Striking")
		CastSpellByName("Heroic Strike") -- Heroic strike doesn't count against the "one thing per run" limit
	end

	if options.doSwitchStance or (options.doOverpower and not CanOverpower(options.OPTimeThresh)) then
		_,_,IsInBerserkerStance = GetShapeshiftFormInfo(3)
		if not IsInBerserkerStance and (UnitMana("player") < 25 or SpellCooldown("Bloodthirst") > TimeUntilMHWillLand()) then
			-- DEFAULT_CHAT_FRAME:AddMessage("WT: Going to Berserker Stance for 3% crit")
			CastSpellByName("Berserker Stance")
			return
		end
	end

	if options.doAnni and UnitExists("target") and not UnitIsFriend("player", "target") then
		WT_TLTP:SetInventoryItem("player",16)
		local currentWeapon = WT_TLTPTextLeft1:GetText()
		if TargetNeedsAnnihilator(options.anniThresh, WT.annihilator_blacklist, WT.annihilator_whitelist) then
			if currentWeapon ~= "Annihilator" then
				DEFAULT_CHAT_FRAME:AddMessage("WT: Equipping Annihilator instead of " .. currentWeapon)
				WT.myWeapon = currentWeapon
				UseItemByName("Annihilator")
				return
			end
		else
			if currentWeapon == "Annihilator" and WT.myWeapon ~= nil and WT.myWeapon ~= "Annihilator" then
				DEFAULT_CHAT_FRAME:AddMessage("WT: Reequipping " .. WT.myWeapon .. " instead of Annihilator")
				UseItemByName(WT.myWeapon)
				return
			end
		end
	end

	if options.doSunder and TargetNeedsSunder(options.sunderThresh, WT.sunder_blacklist, WT.sunder_whitelist) and SpellCooldown("Sunder Armor") == 0 then
		-- DEFAULT_CHAT_FRAME:AddMessage("WT: Sundering")
		CastSpellByName("Sunder Armor")
		return
	end

	if options.doExecute and CanExecute() and SpellCooldown("Execute") == 0 then
		-- DEFAULT_CHAT_FRAME:AddMessage("WT: Executing")
		CastSpellByName("Execute")
		return
	end

	if options.doBloodthirst and SpellCooldown("Bloodthirst") == 0 and not (options.doOverpower and CanOverpower(options.OPTimeThresh) and UnitMana("player") < options.OPAnywayThresh and TimeUntilMHWillLand() > 1.5 and SpellCooldown("Overpower") == 0) then
		-- DEFAULT_CHAT_FRAME:AddMessage("WT: Bloodthirsting")
		CastSpellByName("Bloodthirst")
		return
	end

	_,_,IsInBattleStance = GetShapeshiftFormInfo(1)
	if options.doOverpower and ((SpellCooldown("Bloodthirst") >= 1.5 or not options.doBloodthirst) or UnitMana("player") < options.OPAnywayThresh and TimeUntilMHWillLand() > 1.5) and CanOverpower(options.OPTimeThresh) and (UnitMana("player") < options.OPRageThresh or IsInBattleStance) and SpellCooldown("Overpower") == 0 then
		if not IsInBattleStance then
			-- DEFAULT_CHAT_FRAME:AddMessage("WT: Going to Battle Stance")
			CastSpellByName("Battle Stance")
			return
		end
		-- DEFAULT_CHAT_FRAME:AddMessage("WT: Overpowering")
		CastSpellByName("Overpower")
		return
	end

	if options.doWhirlwind and (SpellCooldown("Bloodthirst") >= 1.5 or not options.doBloodthirst) and SpellCooldown("Whirlwind") == 0 and UnitExists("target") then
		_,_,IsInBerserkerStance = GetShapeshiftFormInfo(3)
		if not IsInBerserkerStance then
			-- DEFAULT_CHAT_FRAME:AddMessage("WT: Going to Berserker Stance in order to Whirlwind")
			CastSpellByName("Berserker Stance")
			return
		end
		-- DEFAULT_CHAT_FRAME:AddMessage("WT: Whirlwinding")
		CastSpellByName("Whirlwind")
		return
	end

	if options.doHamstring and SpellCooldown("Bloodthirst") >= 1.5 and SpellCooldown("Whirlwind") >= 1.5 and TimeUntilMHWillLand() > options.hamstringSwingThresh and UnitMana("player") > options.hamstringRageThresh and SpellCooldown("Hamstring") == 0 then
		-- DEFAULT_CHAT_FRAME:AddMessage("WT: Hamstringing")
		CastSpellByName("Hamstring")
		return
	end
end
